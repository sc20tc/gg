from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
import logging

app = Flask(__name__)
app.config.from_object('config')
app.config['SECRET_KEY'] = '*&*^*&&%^'
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
logging.basicConfig(filename='log.log', level=logging.DEBUG)



from app import views, models
