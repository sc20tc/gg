from app import app, db, models, bcrypt
from flask import render_template, request, url_for, redirect, flash, session
from datetime import datetime
from .forms import SignUp, Login, DateForm, ExerciseForm, ChangePassword
import logging

def usernameInSession(logging_level):
    if 'username' in session:
        return True
    else:
        if logging_level == 'WARNING':
            logging.warning('Username and email not in session')
        elif logging_level == 'ERROR':
            logging.warning('Username and email not in session')
        return False 

def dt_to_int(dt_time):
    return 10000*dt_time.year + 100*dt_time.month + dt_time.day

@app.route('/delete/<int:id>', methods = ['GET', 'POST'])
def delete(id):
    if not usernameInSession('ERROR'):
        return redirect(url_for('login'))
    user = models.User.query.filter_by(email=session['email']).first()
    if not user:
        logging.error('No user with session email found in database')
        return redirect(url_for('login'))
    exercise = models.Exercise.query.filter_by(id=id).first()
    if not exercise:
        logging.warning('No exercise found to be deleted')
        flash('No matching exercise to be deleted!')
        return redirect('/calendar/0')
    else:
        db.session.delete(exercise)
        db.session.commit()
        flash('Exercise deleted successfully!')
        return redirect('/calendar/0')

@app.route('/modify/<int:id>', methods = ['GET', 'POST'])
def modify(id):
    if not usernameInSession('ERROR'):
        flash('Critical error!')
        return redirect(url_for('login'))
    user = models.User.query.filter_by(email=session['email']).first()
    if not user:
        logging.error('No user with session email found in database')
        return redirect(url_for('login'))

    exercise = models.Exercise.query.filter_by(id=id).first()
    if not exercise:
        logging.warning('No exercise found to be deleted')
        flash('No matching exercise to modify!')
        return redirect('/calendar/0')
    else:
        form = ExerciseForm()
        if request.method == 'GET':
            
            form.title.data = exercise.title
            form.weight.data = exercise.weight
            form.reps.data = exercise.reps
            form.description.data = exercise.description
            form.date.data = exercise.date
            return render_template('exercises.html', form=form)
        else:
            if form.validate_on_submit():
                exercise.title = form.title.data
                exercise.weight = form.weight.data
                exercise.reps = form.reps.data
                exercise.description = form.description.data
                exercise.date = form.date.data
                exercise.dateid = dt_to_int(form.date.data)
                db.session.commit()
                flash('Exercise successfully modified - ' + datetime.utcnow().strftime('%H:%M:%S'))
                return displayDay(exercise.dateid)
            



@app.route('/changepassword', methods=['GET', 'POST'])
def changePassword():
    if not usernameInSession('ERROR'):
        flash('Critical error has occured!')
        return redirect(url_for('login'))
    form = ChangePassword()
    if request.method == 'GET':
        return render_template('change_password.html', form=form)
    else:
        user = models.User.query.filter_by(email=session['email']).first()
        if not user:
            logging.error('No user with session email found in database')
            flash('Critical error: could not find user in db!')
            return redirect(url_for('login'))
        if form.validate_on_submit():
            encrypted_password = bcrypt.generate_password_hash(form.password1.data).decode('utf-8') 
            user.password = encrypted_password
            db.session.commit()
            flash('Password changed successfully!')
            return redirect('/calendar/0')
    return redirect('/calendar/0')


@app.route('/displayday/<int:dateid>')
def displayDay(dateid):
    if not usernameInSession('ERROR'):
        return redirect(url_for('login'))
    user = models.User.query.filter_by(email=session['email']).first()
    if not user:
        logging.error('No user with session email found in database')
        return redirect(url_for('login'))
    exercise1 = models.Exercise.query.filter_by(u_id=user.id, dateid=dateid).first()
    if exercise1:
        exercises = models.Exercise.query.filter_by(u_id=user.id, dateid=dateid)
        return render_template('day.html', exercises=exercises)
    else:
        logging.warning('No exercise entries on selected day')
        flash('No entries on that date!')
        return redirect('/calendar/0')

@app.route('/add', methods=['GET', 'POST'])
def add():
    if not usernameInSession('WARNING'):
        flash('Please login to add an exercise entry!')
        return redirect(url_for('login'))
    if 'pg' in session:
        session.pop('pg', None)
    if 'inc' in session:
        session.pop('inc', None)
    form = ExerciseForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            user = models.User.query.filter_by(email=session['email']).first()
            if user != None:
                ex = models.Exercise(u_id=user.id,title=form.title.data,reps=form.reps.data, weight=form.weight.data, description=form.description.data, date=form.date.data, dateid=dt_to_int(form.date.data))
                db.session.add(ex)
                db.session.commit()
                flash('Exercise successfully added - ' + datetime.utcnow().strftime('%H:%M:%S'))
            else:
                logging.warning('No user with session email found in database')
    return render_template('exercises.html', form=form)

@app.route('/calendar/<int:next>', methods=["GET", "POST"])
def calendar(next):
    if not 'inc' in session:
        session['inc'] = 1
    if not 'pg' in session:
        session['pg'] = 0
    if next == 1:
        session['pg'] += session['inc']
    elif next == 2:
        session['pg'] -= 1
    if session['pg'] < 0:
        flash('You are on the first page!')
        session['pg'] = 0
    if not usernameInSession('WARNING'):
        flash('Please login to access your calendar!')
        return redirect(url_for('login'))

    form = DateForm()
    if request.method == "POST":
        if form.validate_on_submit():
            user = models.User.query.filter_by(email=session['email']).first()
            if user != None:
                dt = dt_to_int(form.date.data)
                form.date.data = None
                searchResults = models.Exercise.query.filter_by(u_id=user.id, dateid=dt)
                if searchResults != None:
                    return displayDay(dt)
                else:
                    return redirect('/calendar/0')
            else:
                logging.error('No user with session email found in database')
            
    else:
        user = models.User.query.filter_by(email=session['email']).first()
        if user != None:
            exercises = models.Exercise.query.filter_by(u_id=user.id)
            dateids = []
            dates = []
            for ex in exercises:
                if ex.dateid not in dateids:
                    dateids.append(ex.dateid)
                    dates.append(ex)
            if len(dates) < session['pg']*8:
                flash('There is no next page!')
                session['inc'] = 0
                session['pg']-=1
            else:
                session['inc'] = 1
            return render_template('calendar.html', form=form, days = dates[session['pg']*8:((session['pg']+1)*8)])
        else:
            logging.error('No user with session email found in database')
    return render_template("calendar.html", form=form)

# @app.route('/users')
def users():
    users = models.User.query.order_by(models.User.id).all()
    return render_template('users.html', users=users)

@app.route('/', methods=['GET', 'POST'])
# app.logger.info('index route request')
def login():
    if 'pg' in session:
        session.pop('pg', None)
    if 'inc' in session:
        session.pop('inc', None)
    if 'username' in session:
        logging.warning('Multiple login attempts')
        flash("Hi, " + session['username'] + ". You are already logged in!")
        return redirect('/calendar/0')
    form = Login()
    if request.method == 'GET':
        return render_template('login.html', form=form)
    elif request.method == 'POST':
        if form.validate_on_submit():
            user_object = models.User.query.filter_by(email=form.email.data).first()
            if not user_object:
                logging.warning('Unsuccessful login attempt - no account registered with email entered')
                flash('No user with that email is registered!')
                return render_template('login.html', form=form)
            if user_object and bcrypt.check_password_hash(user_object.password, form.password.data):
                # session['user'] = user_object
                session['username'] = user_object.username
                session['email'] = user_object.email
                flash('Welcome, '+ session['username'] + '!')
                return redirect('/calendar/0')
            if (user_object and (not bcrypt.check_password_hash(user_object.password, form.password.data))):
                logging.warning('Unsuccessful login attempt - incorrect password')
                flash('Incorrect password!')
                return render_template('login.html', form=form)
        else:
            flash('Failed to submit form!')
            return render_template('login.html', form=form)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if usernameInSession('WARNING'):
        flash('You are already logged in so cannot sign up again!')
        return redirect('/calendar/0')
    form = SignUp()
    if request.method == 'GET':
        return render_template('signup.html', form=form)
    if request.method == 'POST':
        if form.validate_on_submit():
            encrypted_password = bcrypt.generate_password_hash(form.password1.data).decode('utf-8')
            new_user = models.User(username=form.username.data, email=form.email.data, password=encrypted_password)
            session['username'] = form.username.data
            session['email'] = form.email.data
            db.session.add(new_user)
            db.session.commit()
            return redirect('/calendar/0')

        # flash("Sign Up Successful! Welcome, " + session['username'] + '.')
        else:
            flash('Failed to submit form!')
            return render_template('signup.html', form=form)


@app.route('/logout')
def logout():
    logout = 0
    if 'username' in session:
        session.pop('username', None)
        logout = 1
    if 'email' in session:
        session.pop('email', None)

    if logout:
        logging.debug('User successfully logged out')
        flash('You have successfully been logged out!')
        return redirect(url_for('login'))
    else:
        logging.warning('Signout attempted when no user is signed in')
        flash('You must be signed in to log out!')
        return redirect(url_for('login'))


