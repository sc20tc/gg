from flask_wtf import FlaskForm
from wtforms import IntegerField
from wtforms import PasswordField, StringField, SubmitField, DateField
from wtforms.validators import DataRequired, EqualTo, Length, ValidationError
from app import models
from datetime import datetime



class ChangePassword(FlaskForm):
        password1 = PasswordField('Enter new password', validators=[DataRequired(), Length(max=60)])
        password2 = PasswordField('Confirm new password', validators=[DataRequired(), Length(max=60) , EqualTo('password1', message='Passwords must match!')])
        submit = SubmitField('Submit')
        def validate_password1(self, password1):
            if len(password1.data) < 8:
                raise ValidationError('Password must cointain more than 7 characters!')

class SignUp(FlaskForm):
        username = StringField('Enter username', validators=[DataRequired(), Length(max=50)])
        email = StringField('Enter email', validators=[DataRequired(), Length(max=50)])
        password1 = PasswordField('Enter password', validators=[DataRequired(), Length(max=60)])
        password2 = PasswordField('Confirm password', validators=[DataRequired(), Length(max=60), EqualTo('password1', message='Passwords must match!')])
        submit = SubmitField('Submit')

        def validate_email(self, email):
                at = 0
                dot = 0
                for c in email.data:
                        if c == '@':
                                at = 1
                        elif c == '.':
                                dot = 1
                if not at or not dot:
                        raise ValidationError('Invalid email address!!')
                        
                user_object = models.User.query.filter_by(email=email.data).first()
                if user_object:
                        raise ValidationError('This email address is already registered!')
        
        def validate_password1(self, password1):
                if len(password1.data) < 8:
                        raise ValidationError('Password must cointain more than 7 characters!')
        


class Login(FlaskForm):
        email = StringField('Enter email', validators=[DataRequired(), Length(max=50)])
        password = PasswordField('Enter password', validators=[DataRequired(), Length(max=60)])
        submit = SubmitField('Submit')

class DateForm(FlaskForm):
        date = DateField('Date', validators=[DataRequired()])
        submit = SubmitField('Submit')

class ExerciseForm(FlaskForm):
        title = StringField('Name of exericse', validators=[DataRequired(), Length(max=50)])
        weight = IntegerField('Weight used (kg)', validators=[DataRequired()])
        reps = IntegerField('Number of reps completed', validators=[DataRequired()])
        description = StringField('Additional information / comments', validators=[Length(max=50)])
        date = DateField('Date', default = datetime.utcnow(), validators=[DataRequired()])
        submit = SubmitField('Submit')
